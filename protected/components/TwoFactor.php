<?php
require Yii::getPathOfAlias('application').'/vendor/autoload.php';
use PragmaRX\Google2FAQRCode\Google2FA;

class TwoFactor extends CApplicationComponent {

    private $google2fa;
    public $window = 4;

	public function init() {
        parent::init();
        $this->google2fa = new Google2FA();
    }

    public function generateSecret() {
    	
        return $this->google2fa->generateSecretKey();
    }

    public function checkCode($secret, $code, $window = null) {
    	$window = $window === null ? $this->window : $window;
        return $this->google2fa->verifyKey($secret, $code, $window);
    }

    public function generateQrCodeInline($companyName, $username, $secretKey) {
        return $this->google2fa->getQRCodeInline(
            $companyName,
            $username,
            $secretKey
        );
    }
}
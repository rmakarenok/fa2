<?php

class FactorController extends Controller
{
	public $layout='column1';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	/**
	 * This is the main action.
	 */
	public function actionIndex()
	{
	    $this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		
		if(isset(Yii::app()->session['loginfa2'])){

			$model = null;
			$codeform = new CheckFactorForm;
			if(Yii::app()->request->isPostRequest && isset($_POST['CheckFactorForm']))
			{
				$codeform->secret = $_POST['CheckFactorForm']['secret'];
				$codeform->code = $_POST['CheckFactorForm']['code'];
				$model = new LoginForm;
				$model->email = Yii::app()->session['loginfa2']['email'];
				$model->password = Yii::app()->session['loginfa2']['password'];
				if($codeform->validate() && $model->validate() && $model->login()){
					unset(Yii::app()->session['loginfa2']);
					$this->redirect(Yii::app()->homeUrl);
				}
			}

		} else {

			$model = new LoginForm;
			$codeform = null;
			// if it is ajax validation request
			/*
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
			*/
			// collect user input data
			if(isset($_POST['LoginForm']))
			{
				$model->attributes = $_POST['LoginForm'];
				// validate user input and redirect to the previous page if valid
				if($model->validate()){
					$_POST['LoginForm']['secret'] = $model->secret;
					Yii::app()->session['loginfa2'] = $_POST['LoginForm'];
					$codeform = new CheckFactorForm;
				}
			}
	    }
		// display the login form
		$this->render('login',array('model'=>$model, 'codeform'=>$codeform));
	}

	/**
	 * Displays the registration page
	 */
	public function actionRegistration()
	{

		if(isset(Yii::app()->session['fa2'])){

 			$user = null;
			$codeform = new CheckFactorForm;
			if(Yii::app()->request->isPostRequest && isset($_POST['CheckFactorForm']))
			{  
				$codeform->secret = $_POST['CheckFactorForm']['secret'];
				$codeform->code = $_POST['CheckFactorForm']['code'];
				$user = new RegistrationForm;
				$user->username = Yii::app()->session['fa2']['username'];
				$user->email = Yii::app()->session['fa2']['email'];
				$user->password = Yii::app()->session['fa2']['password'];
				$user->secret = Yii::app()->session['fa2']['secret'];
				if($codeform->validate() && $user->validate()) {
					$user->save();
					$identity = new UserIdentity($user->email, Yii::app()->session['fa2']['password']);
					if($identity->authenticate()){
						Yii::app()->user->login($identity);
						unset(Yii::app()->session['fa2']);
						$this->redirect(Yii::app()->homeUrl);
					}
				}
			}
			
	    } else {
	    	$codeform = null;
			$user = new RegistrationForm;

			// if it is ajax validation request
			/*	
			if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
			{
				echo CActiveForm::validate($user);
				Yii::app()->end();
			}
			*/
			if(Yii::app()->request->isPostRequest && isset($_POST['RegistrationForm']))
			{

	            $user->attributes = $_POST['RegistrationForm'];
	       
				if($user->validate()) {
					$user->secret = Yii::app()->twoFactor->generateSecret();
					$_POST['RegistrationForm']['secret'] = $user->secret;
					Yii::app()->session['fa2'] = $_POST['RegistrationForm'];
					$codeform = new CheckFactorForm;
				}
			}

		}

		// display the registration form
		$this->render('registration',array('model'=>$user,'codeform'=>$codeform));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}

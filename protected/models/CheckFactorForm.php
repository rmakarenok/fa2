<?php

/**
 * CheckFactorForm class.
 * CheckFactorForm is the data structure for keeping
 * 2FA code form data.
 */
class CheckFactorForm extends CFormModel 
{

    public $secret;
    public $code;

    public function rules() {
        $rules = array(
            array('code', 'filter', 'filter' => 'strip_tags'),
            array('code', 'filter','filter' =>'trim'),
            array('secret, code', 'required'),
            array('code', 'length', 'min' => 6),
            array('code', 'validateCode'),
        );
        return $rules;
    }

    public function validateCode($attribute, $params) {
        if(!Yii::app()->twoFactor->checkCode($this->secret, $this->code)){
            $this->addError($attribute, 'Incorrect code.');
        }
    }

}
?>
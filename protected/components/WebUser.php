<?php
class WebUser extends CWebUser
{
	
	private $_model;

	public function init()
    {
    	parent::init();
    }
    
	public function getUsername()
    {
        $this->_loadModel();
        return $this->_model->username;
    }

    public function getEmail()
    {
        $this->_loadModel();
        return $this->_model->email;
    }

    public function getSecret()
    {
        $this->_loadModel();
        return $this->_model->secret;
    }

	private function _loadModel()
    {
        if(!$this->_model)
            $this->_model = User::model()->findByPk($this->id);
        if(!$this->_model){
            Yii::app()->user->logout();
            Yii::app()->request->redirect('/');
            Yii::app()->end;
        }
    }
       
    public function getModel()
    {
        $this->_loadModel();
        return $this->_model;
    }
}
?>
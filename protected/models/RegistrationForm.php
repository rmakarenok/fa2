<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class RegistrationForm extends User {
    
    public $verifyPassword;
    public $verifyCode;

    public function rules() {
        $rules = array(
            array('username, email, password', 'filter', 'filter' => 'strip_tags'),
            array('username, email, password', 'filter','filter' =>'trim'),
            array('username', 'required'),
            array('password', 'required'),
            array('email', 'required'),
            array('username, email', 'length', 'max' => 128, 'min' => 3),
            array('password', 'length', 'max' => 64, 'min' => 3),
            array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => "Only latin characters"),
            array('username', 'checkUsername'),
            array('email', 'email'),
            array('email', 'checkEmail'), 
        );
         return $rules;
    }
    
    public function checkUsername($attr)
    {
        $labels = $this->attributeLabels();
        $value = (string)($this->$attr);
        $check = User::model()->find(array('condition'=>"LOWER({$attr})=:{$attr}",'params'=>array(":{$attr}"=>mb_strtolower($value, 'UTF-8'))));
        if($check){
            $this->addError($attr, $labels[$attr] .' is taken');
            return false;
        }
        return true;
    }
    
    public function checkEmail($attr)
    {
        $labels = $this->attributeLabels();
        $value = (string)($this->$attr);
        $check = User::model()->find(array('condition'=>"LOWER({$attr})=:{$attr}",'params'=>array(":{$attr}"=>mb_strtolower($value, 'UTF-8'))));
        if($check){
            $this->addError($attr, $labels[$attr] .' is taken');
            return false;
        }
        return true;
    }

    public function attributeLabels() {
        return array(
            'username' => 'Name',
            'password' => 'Password',
            'verifyPassword' => 'Confirm password',
            'email' => 'Email',
            'verifyCode' => 'Verify code',
        );
    }
}
<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Two Factor Authentication',
	'theme'=>'classic',
    'homeUrl'=>array('factor/index'),

	// preloading 'log' component
    'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

   'defaultController'=>'factor',

	// application components
	'components'=>array(
		'user'=>array(
			'class' => 'WebUser',
			'allowAutoLogin'=>false,
		),
		'db'=>array(
			'connectionString' => 'sqlite:'. dirname(__FILE__)."/../data/sqlite.db",
			'tablePrefix' => 'tbl_',
		),
		'twoFactor'=>array(
            'class'=>'TwoFactor',
        ), 
		'urlManager'=>array(
		    'urlFormat'=>'path',
		    'showScriptName' => false,
			'rules'=>array(
				'/' => array('factor/index'),
				'<action:(login|logout|registration)>' => 'factor/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'errorHandler'=>array(
			'errorAction'=>'factor/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

);
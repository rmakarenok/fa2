<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<?php
if(isset(Yii::app()->session['loginfa2'])){

    $QRcodeURL = Yii::app()->twoFactor->generateQrCodeInline(
           Yii::app()->name,
           Yii::app()->session['loginfa2']['email'],
           Yii::app()->session['loginfa2']['secret']);

?>
<p>Set up you 2FA by scanning the barcode bellow. Alternatively, you can use the code <?php echo Yii::app()->session['loginfa2']['secret']; ?></p>
<p><img src="<?php echo $QRcodeURL; ?>" /></p>
<p>You must set up your Google Authenticator app before continuing. You will be unable to login otherwise.

<div class="form">
<?php 

$form2 = $this->beginWidget('CActiveForm', array(
    'id'=>'login-form-fa',
    'enableAjaxValidation'=>false,
)); ?>

Please, enter the 6 numbers, shown in your phone<br>
<?php echo $form2->hiddenField($codeform,'secret',array('value'=>Yii::app()->session['loginfa2']['secret'])); ?>
<div class="row">
    <?php echo $form2->labelEx($codeform,'code'); ?>
    <?php echo $form2->textField($codeform,'code'); ?>
    <?php echo $form2->error($codeform,'code'); ?>
</div>

<div class="row submit">
    <?php echo CHtml::submitButton('Complete Registration'); ?>
</div>  
<?php $this->endWidget(); ?>
</div>

<?php 

} else {

?>

<p>Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<?php 
}
?>

<?php
$this->pageTitle=Yii::app()->name . ' - Registration';
$this->breadcrumbs=array(
    'Registration',
); ?>

<h1>Registration</h1>

<?php
if(isset(Yii::app()->session['fa2'])){

    $QRcodeURL = Yii::app()->twoFactor->generateQrCodeInline(
           Yii::app()->name,
           Yii::app()->session['fa2']['email'],
           Yii::app()->session['fa2']['secret']);

?>
<p>Set up you 2FA by scanning the barcode bellow. Alternatively, you can use the code <?php echo Yii::app()->session['fa2']['secret']; ?></p>
<p><img src="<?php echo $QRcodeURL; ?>" /></p>
<p>You must set up your Google Authenticator app before continuing. You will be unable to login otherwise.

<div class="form">
<?php 

$form2 = $this->beginWidget('CActiveForm', array(
    'id'=>'registration-form-fa',
    'enableAjaxValidation'=>false,
)); ?>

Please, enter the 6 numbers, shown in your phone<br>
<?php echo $form2->hiddenField($codeform,'secret',array('value'=>Yii::app()->session['fa2']['secret'])); ?>
<div class="row">
    <?php echo $form2->labelEx($codeform,'code'); ?>
    <?php echo $form2->textField($codeform,'code'); ?>
    <?php echo $form2->error($codeform,'code'); ?>
</div>

<div class="row submit">
    <?php echo CHtml::submitButton('Complete Registration'); ?>
</div>  
<?php $this->endWidget(); ?>
</div>

<?php 

} else {

?>

<p>Please fill out the following form to register:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registration-form',
    'enableAjaxValidation'=>false,
)); ?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<div class="row">
    <?php echo $form->labelEx($model,'username'); ?>
    <?php echo $form->textField($model,'username'); ?>
    <?php echo $form->error($model,'username'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'email'); ?>
    <?php echo $form->textField($model,'email'); ?>
    <?php echo $form->error($model,'email'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'password'); ?>
    <?php echo $form->textField($model,'password'); ?>
    <?php echo $form->error($model,'password'); ?>
</div>

<div class="row submit">
    <?php echo CHtml::submitButton('Register'); ?>
</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<?php 
}
?>
              